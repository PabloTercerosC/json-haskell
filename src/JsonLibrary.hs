module JsonLibrary
    ( jsonLib
    ) where

import JsonObject
import JsonBuilder
import JsonWriter

completeValue :: Maybe JsonValue
completeValue = Just (JObject [("\"test1\"",Just (JString "test")),("\"object1\"",Just (JObject [("\"test2\"",Just (JNumber 2.0)),("\"object2\"",Just (JObject [("\"name\"",Just (JString "value")),("\"test3\"",Just (JString "value2"))]))]))])

completeString :: String
completeString = "{\"test1\":\"test\",\"object1\":{\"test2\":2,\"object2\":{\"name\":\"value\",\"test3\":\"value2\"}}}"

jsonLib :: IO ()
jsonLib = do {putStrLn (writeJson completeValue); print (parseJson completeString)}