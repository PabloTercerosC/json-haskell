module JsonWriter where

import JsonObject
import Data.List ( intercalate )

writeJson :: Maybe JsonValue -> String
writeJson (Just (JString s)) = s
writeJson (Just (JBool b)) = show b
writeJson (Just (JNumber n)) = show n
writeJson (Just (JList l)) = show (map writeJson l)
writeJson (Just (JObject o)) = "{" ++ createFromObject o ++ "}"

showNode :: (String, Maybe JsonValue) -> String
showNode (name,value) = name ++ ": " ++ writeJson value

createFromObject :: [(String, Maybe JsonValue)] -> String
createFromObject [] = ""
createFromObject xs = intercalate ", " (map showNode xs)