{-# LANGUAGE LambdaCase #-}
module JsonBuilder where

import JsonObject
import Data.Char
import Control.Applicative (Alternative, empty, (<|>))

--------------------------------------------------------------------------------------------------------------------------
-- PARSER DEFINITION

newtype Parser a = Parser {parse :: String -> Maybe (a, String)}

instance Functor Parser where
    fmap g (Parser a) = Parser (\x -> case a x of
            Just (a, xs) -> Just (g a, xs)
            Nothing -> Nothing)

instance Applicative Parser where
  pure a = Parser (\s -> Just (a, s))
  (<*>) (Parser f) (Parser g) = Parser (\s -> do
      (ab, s') <- f s
      (a, s'') <- g s'
      Just (ab a, s'')
    )

instance Alternative Parser where
    empty = Parser (const Nothing)
    (Parser a) <|> (Parser b) = Parser (\x -> a x <|> b x)

--------------------------------------------------------------------------------------------------------------------------
-- SIMPLE VALUES

parserBool :: Parser JsonValue
parserBool = Parser (\case
    ('f' : 'a' : 'l' : 's' : 'e' : xs) -> Just (JBool False, xs)
    ('t' : 'r' : 'u' : 'e' : xs) -> Just (JBool True, xs)
    _ -> Nothing)

parserNumber :: Parser JsonValue
parserNumber = Parser (\s -> case span isDigit'' s of
  ("", _) -> Nothing
  (ds, rs) -> Just (JNumber (read ds), rs))

isDigit'' :: Char -> Bool
isDigit'' = isDigit

parserString :: Parser JsonValue
parserString = Parser (\case
  ('\"' : xs) -> let (str, rest) = span (/= '\"') xs
                     rest' = drop 1 rest
                 in Just (JString str, rest')
  _ -> Nothing)

--------------------------------------------------------------------------------------------------------------------------
-- PARSE LIST

parserList :: Parser JsonValue
parserList = Parser (\x -> if (head . trim) x == '['
    then case span' x ']' (sumOpenBrackets x '[' ']') of
    ("", _) -> Nothing
    (str, rest) -> Just (JList (parserList' (removeFirstAndLast str)), rest)
    else Nothing)

parserList' :: String -> [Maybe JsonValue]
parserList' "" = []
parserList' str = if (head . trim) str == '['
  then case span' str ']' (sumOpenBrackets str '[' ']') of
    (str, rest) -> extractValue (getJValue str) : parserList' (dropWhile (==',') rest)
  else case span (/=',') str of
    (str, rest) -> extractValue (getJValue str) : parserList' (dropWhile (==',') rest)

--------------------------------------------------------------------------------------------------------------------------
-- PARSE OBJECT

parserObject :: Parser JsonValue
parserObject = Parser (\x -> if (head . trim) x == '{'
    then case span' x '}' (sumOpenBrackets x '{' '}') of
    ("", _) -> Nothing
    (str, rest) -> Just (JObject (parserObject' (removeFirstAndLast str)), rest)
    else Nothing)

parserObject' :: String -> [(String, Maybe JsonValue)]
parserObject' "" = []
parserObject' str = if foundBraceBeforeComma str
  then case span' str '}' (sumOpenBrackets str '{' '}') of
  (str, rest) -> buildTuple str : parserObject' (dropWhile (==',') rest)
  else case span (/=',') str of
  (str, rest) -> buildTuple str : parserObject' (dropWhile (==',') rest)

--------------------------------------------------------------------------------------------------------------------------
-- SUPPORTING FUNCTIONS

isOpenChar :: String -> Char -> Bool
isOpenChar str char
            | head str == char = True
            | otherwise = False

isClosedChar :: String -> Char -> Bool
isClosedChar str char
            | last str == char = True
            | otherwise = False

splitAcc :: Char -> Bool -> Maybe String -> String -> [String]
splitAcc _ _ Nothing [] = []
splitAcc _ False Nothing (_:_) = []
splitAcc _ True Nothing (_:_) = []
splitAcc _ _ (Just []) [] = []
splitAcc _ _ (Just []) ys = [reverse ys]
splitAcc c b (Just(x:xs)) ys
            | isOpenChar [x] '[' || isOpenChar [x] '{' = splitAcc c False (Just xs) (x : ys)
            | isClosedChar [x] ']' || isClosedChar [x] '}' = splitAcc c True (Just xs) (x : ys)
            | x == c && b = reverse ys : splitAcc c b (Just xs) []
            | otherwise = splitAcc c b (Just xs) (x : ys)


trim :: String -> String
trim = filter (/= ' ')

removeFirstAndLast :: String -> String
removeFirstAndLast str = drop 1 (take (length str - 1) str)

--------------------------------------------------------------------------------------------------------------------------
-- NEW SUPPORTING FUNCTIONS

span' :: String -> Char -> Int -> (String, String)
span' [] _ _ = ("", "")
span' (x:xs) c sum
  | x == c = let (ys, zs) = span' xs c (sum - 1) in (x:ys, zs)
  | xs == "" || sum == 0 = ("", x:xs)
  | otherwise = let (ys, zs) = span' xs c sum in (x:ys, zs)

sumOpenBrackets :: String -> Char -> Char -> Int
sumOpenBrackets str open close = countBracketsHelper str 0
    where countBracketsHelper [] count = count
          countBracketsHelper (x:xs) count
              | x == open = countBracketsHelper xs (count+1)
              | x == close = count
              | otherwise = countBracketsHelper xs count

extractValue :: Maybe(a, String) -> Maybe a
extractValue (Just (x, _)) = Just x
extractValue Nothing       = Nothing

buildTuple :: String -> (String, Maybe JsonValue)
buildTuple [] = ("", Just (JString ""))
buildTuple s = let x:y:xs = splitAcc ':' True (Just s) [] in (x, extractValue (getJValue y))

foundBraceBeforeComma :: String -> Bool
foundBraceBeforeComma [] = False
foundBraceBeforeComma (x:xs)
  | x == '{' = True
  | x == ',' = False
  | otherwise = foundBraceBeforeComma xs

--------------------------------------------------------------------------------------------------------------------------
-- PARSE JSON

getJValue :: String -> Maybe (JsonValue, String)
getJValue "" = Nothing
getJValue str = parse (parserBool <|> parserNumber <|> parserString <|> parserList <|> parserObject) str

parseJson :: String -> Maybe JsonValue
parseJson [] = Nothing
parseJson s = extractValue (parse parserObject s)